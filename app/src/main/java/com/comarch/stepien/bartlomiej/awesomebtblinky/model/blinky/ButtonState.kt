package com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky

enum class ButtonState {
    PRESSED,
    RELEASED,
    UNKNOWN;

    companion object {
        fun fromResult(result: Byte) =
            when (result) {
                1.toByte() -> PRESSED
                0.toByte() -> RELEASED
                else -> UNKNOWN
            }
    }
}