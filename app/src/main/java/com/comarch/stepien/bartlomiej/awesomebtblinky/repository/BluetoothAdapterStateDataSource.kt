package com.comarch.stepien.bartlomiej.awesomebtblinky.repository

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.BluetoothAdapterState
import com.comarch.stepien.bartlomiej.awesomebtblinky.permissions.Permissions
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Singleton

@Singleton
class BluetoothAdapterStateDataSource(
    private val applicationContext: Context
) {
    private val bluetoothAdapter by lazy { applicationContext.bluetoothAdapter }

    @ExperimentalCoroutinesApi
    val state: Flow<BluetoothAdapterState> = callbackFlow {
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val action = intent?.action
                if (action != BluetoothAdapter.ACTION_STATE_CHANGED) return

                val state = intent.getIntExtra(
                    BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR
                )
                when (state) {
                    BluetoothAdapter.STATE_OFF -> trySend(BluetoothAdapterState.OFF)
                    BluetoothAdapter.STATE_TURNING_OFF -> trySend(BluetoothAdapterState.TURNING_OFF)
                    BluetoothAdapter.STATE_TURNING_ON -> trySend(BluetoothAdapterState.TURNING_ON)
                    BluetoothAdapter.STATE_ON -> trySend(BluetoothAdapterState.ON)
                }
            }
        }

        applicationContext.registerReceiver(receiver, INTENT_FILTER)

        val initialValue = when {
            isEnabled() -> {
                BluetoothAdapterState.ON
            }
            else -> {
                BluetoothAdapterState.OFF
            }
        }
        trySend(initialValue)

        awaitClose {
            applicationContext.unregisterReceiver(receiver)
        }
    }

    private fun isEnabled() = bluetoothAdapter?.isEnabled ?: false

    @SuppressLint("MissingPermission") // checking is handled in PermissionsHelper and UI
    fun enable() {
        if (!Permissions.isConnectPermissionGranted(applicationContext)) {
            throw Permissions.ConnectPermissionMissingException
        }
        bluetoothAdapter?.enable()
    }

    companion object {
        val INTENT_FILTER = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
    }
}
