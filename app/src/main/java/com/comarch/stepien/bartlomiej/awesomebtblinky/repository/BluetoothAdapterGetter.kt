package com.comarch.stepien.bartlomiej.awesomebtblinky.repository

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.os.Build

val Context.bluetoothAdapter: BluetoothAdapter?
    get() = when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
            applicationContext.getSystemService(BluetoothManager::class.java)
        }
        else -> {
            applicationContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        }
    }.adapter
