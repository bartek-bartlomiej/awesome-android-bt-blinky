package com.comarch.stepien.bartlomiej.awesomebtblinky

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication : Application()