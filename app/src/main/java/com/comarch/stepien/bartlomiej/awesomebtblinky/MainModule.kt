package com.comarch.stepien.bartlomiej.awesomebtblinky

import android.content.Context
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.EventBus
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.EventBusImpl
import com.comarch.stepien.bartlomiej.awesomebtblinky.repository.BluetoothAdapterStateDataSource
import com.comarch.stepien.bartlomiej.awesomebtblinky.repository.BluetoothDevicesDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object MainModule {

    @Provides
    fun provideBluetoothDevicesDataSource(@ApplicationContext context: Context): BluetoothDevicesDataSource {
        return BluetoothDevicesDataSource(context)
    }

    @Provides
    fun provideBluetoothAdapterStateDataSource(@ApplicationContext context: Context): BluetoothAdapterStateDataSource {
        return BluetoothAdapterStateDataSource(context)
    }

    @Provides
    fun provideEventBus(): EventBus {
        return EventBusImpl()
    }
}