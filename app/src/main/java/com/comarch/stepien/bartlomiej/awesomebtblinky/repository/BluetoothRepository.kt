package com.comarch.stepien.bartlomiej.awesomebtblinky.repository

import android.bluetooth.le.ScanFilter
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.BluetoothAdapterState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BluetoothRepository @Inject constructor(
    private val bluetoothAdapterStateDataSource: BluetoothAdapterStateDataSource,
    private val bluetoothDevicesDataSource: BluetoothDevicesDataSource
) {

    @ExperimentalCoroutinesApi
    val adapterState: Flow<BluetoothAdapterState>
        get() = bluetoothAdapterStateDataSource.state

    fun enableBluetooth() = bluetoothAdapterStateDataSource.enable()

    @ExperimentalCoroutinesApi
    val blinkyDevices
        get() = bluetoothDevicesDataSource.getDevices(listOf(BLINKY_FILTER))

    fun getBlinkyAdapter(address: String) = bluetoothDevicesDataSource.getDeviceAdapter(address)

    companion object {
        private val BLINKY_FILTER = ScanFilter.Builder()
            .setDeviceName("Blinky Example")
            .build()
    }
}