package com.comarch.stepien.bartlomiej.awesomebtblinky.permissions

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat

object Permissions {

    val scanPermission = when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> Manifest.permission.BLUETOOTH_SCAN
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> Manifest.permission.ACCESS_FINE_LOCATION
        else -> Manifest.permission.ACCESS_COARSE_LOCATION
    }

    val connectPermission = when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> Manifest.permission.BLUETOOTH_CONNECT
        else -> null
    }

    fun isConnectPermissionGranted(context: Context): Boolean =
        connectPermission?.let {
            ContextCompat.checkSelfPermission(
                context,
                it
            ) == PackageManager.PERMISSION_GRANTED
        } ?: true

    fun isScanPermissionGranted(context: Context): Boolean =
        ContextCompat.checkSelfPermission(
            context,
            scanPermission
        ) == PackageManager.PERMISSION_GRANTED

    object ConnectPermissionMissingException :
        Exception("Permission ${connectPermission!!} is missing")

    object ScanPermissionMissingException :
        Exception("Permission $scanPermission is missing")
}
