package com.comarch.stepien.bartlomiej.awesomebtblinky.dao

import android.bluetooth.*
import android.content.Context
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky.BlinkyState
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky.ButtonState
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky.DiodeState
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky.ConnectionStatus
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*

class BlinkyAdapter(
    private val applicationContext: Context,
    private val device: BluetoothDevice
) {
    private lateinit var gatt: BluetoothGatt

    private val _state = MutableStateFlow(BlinkyState(status = ConnectionStatus.Connecting))

    private val processor = GattProcessor()

    @ExperimentalCoroutinesApi
    val state: Flow<BlinkyState>
        get() = callbackFlow {
            val callback = object : BluetoothGattCallback() {
                override fun onConnectionStateChange(
                    gatt: BluetoothGatt?,
                    status: Int,
                    newState: Int
                ) {
                    when (newState) {
                        BluetoothProfile.STATE_CONNECTED -> {
                            _state.update { state -> state.copy(status = ConnectionStatus.Discovering) }
                            gatt?.discoverServices()
                        }
                        BluetoothProfile.STATE_DISCONNECTED -> {
                            close(DeviceDisconnected)
                        }
                    }
                }

                override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
                    when (status) {
                        BluetoothGatt.GATT_SUCCESS -> {
                            _state.update { state -> state.copy(status = ConnectionStatus.Ready) }

                            getCharacteristic(DIODE_CHARACTERISTIC)?.apply {
                                processor.queueRead(gatt, this)
                            }
                            getCharacteristic(BUTTON_CHARACTERISTIC)?.apply {
                                processor.queueNotify(gatt, this)
                            }
                        }
                        else -> {
                            close(ServicesNotDiscovered)
                        }
                    }
                }

                override fun onCharacteristicChanged(
                    gatt: BluetoothGatt?,
                    characteristic: BluetoothGattCharacteristic?
                ) {
                    processor.handleCommandProcessed()
                    characteristic?.let {
                        if (it.uuid != BUTTON_CHARACTERISTIC) return

                        _state.update { state ->
                            state.copy(
                                buttonState = ButtonState.fromResult(
                                    it.value[0]
                                )
                            )
                        }
                    }
                }

                override fun onCharacteristicRead(
                    gatt: BluetoothGatt?,
                    characteristic: BluetoothGattCharacteristic?,
                    status: Int
                ) {
                    processor.handleCommandProcessed()
                    characteristic?.let {
                        if (it.uuid != DIODE_CHARACTERISTIC) return

                        _state.update { state ->
                            state.copy(
                                diodeState = DiodeState.fromResult(
                                    it.value[0]
                                )
                            )
                        }
                    }
                }

                override fun onCharacteristicWrite(
                    gatt: BluetoothGatt?,
                    characteristic: BluetoothGattCharacteristic?,
                    status: Int
                ) {
                    processor.handleCommandProcessed()
                    when (status) {
                        BluetoothGatt.GATT_SUCCESS -> {
                            characteristic!!.let {
                                if (it.uuid != DIODE_CHARACTERISTIC) return

                                _state.update { state ->
                                    state.copy(
                                        diodeState = DiodeState.fromResult(
                                            it.value[0]
                                        )
                                    )
                                }
                            }
                        }
                        else -> {
                            _state.update { state ->
                                val diodeState = state.diodeState
                                val previousState =
                                    if (diodeState == DiodeState.ENABLING) DiodeState.DISABLED else DiodeState.ENABLED
                                state.copy(diodeState = previousState)
                            }
                        }
                    }
                }
            }

            gatt = device.connectGatt(applicationContext, false, callback)
            launch {
                _state.asStateFlow().collect {
                    trySend(it)
                }
            }

            _state.update {
                BlinkyState(status = ConnectionStatus.Connecting)
            }

            awaitClose {
                gatt.close()
            }
        }

    fun toggleDiode() {
        val diodeState = _state.value.diodeState
        if (diodeState == DiodeState.ENABLING || diodeState == DiodeState.DISABLING) return

        getCharacteristic(DIODE_CHARACTERISTIC)?.apply {
            value = byteArrayOf(
                if (diodeState == DiodeState.ENABLED) 0 else 1
            )
            processor.queueWrite(gatt, this)
            _state.update { state ->
                val nextState =
                    if (diodeState == DiodeState.ENABLED) DiodeState.DISABLING else DiodeState.ENABLING
                state.copy(diodeState = nextState)
            }
        }
    }

    private fun getCharacteristic(uuid: UUID): BluetoothGattCharacteristic? {
        val gattService = gatt.getService(BLINKY_EXAMPlE_SERVICE)
        return gattService?.getCharacteristic(uuid)
    }

    companion object {
        private val BLINKY_EXAMPlE_SERVICE = UUID.fromString("de8a5aac-a99b-c315-0c80-60d4cbb51224")
        private val DIODE_CHARACTERISTIC = UUID.fromString("5b026510-4088-c297-46d8-be6c736a087a")
        private val BUTTON_CHARACTERISTIC = UUID.fromString("61a885a4-41c3-60d0-9a53-6d652a70d29c")

        sealed class ConnectionFailedException : Exception()
        object DeviceDisconnected : ConnectionFailedException()
        object ServicesNotDiscovered : ConnectionFailedException()
    }
}