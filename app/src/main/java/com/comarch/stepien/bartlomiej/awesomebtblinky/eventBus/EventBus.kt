package com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import java.util.*

open class Event {
    val id: String = UUID.randomUUID().toString()
}

interface EventDispatcher {
    val events: StateFlow<List<Event>>
    fun eventHandled(event: Event)
}

interface EventEmitter {
    fun emitEvent(event: Event)
}

interface EventBus : EventDispatcher, EventEmitter

class EventBusImpl : EventBus {
    private val _events = MutableStateFlow<List<Event>>(listOf())
    override val events: StateFlow<List<Event>>
        get() = _events.asStateFlow()

    override fun emitEvent(event: Event) {
        _events.update {
            it + event
        }
    }

    override fun eventHandled(event: Event) {
        _events.update { list ->
            list.filterNot { it.id == event.id }
        }
    }
}

interface EventHandler {
    val dispatcher: EventDispatcher

    fun handleEvent(event: Event) {
        dispatcher.eventHandled(event)
    }

    fun subscribe(scope: CoroutineScope) {
        dispatcher.events
            .onEach { events ->
                events.firstOrNull()?.let { event -> this.handleEvent(event) }
            }
            .launchIn(scope)
    }
}
