package com.comarch.stepien.bartlomiej.awesomebtblinky.ui.blinky

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.comarch.stepien.bartlomiej.awesomebtblinky.R
import com.comarch.stepien.bartlomiej.awesomebtblinky.databinding.FragmentBlinkyBinding
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.Event
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.EventDispatcher
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.EventHandler
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.BluetoothAdapterState
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky.ButtonState
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky.ConnectionStatus
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky.DiodeState
import com.comarch.stepien.bartlomiej.awesomebtblinky.permissions.Permissions
import com.comarch.stepien.bartlomiej.awesomebtblinky.ui.BluetoothAdapterStateViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class BlinkyFragment : Fragment(R.layout.fragment_blinky), EventHandler {

    private val args: BlinkyFragmentArgs by navArgs()
    private val bluetoothStateViewModel: BluetoothAdapterStateViewModel by activityViewModels()
    private val viewModel: BlinkyViewModel by viewModels()

    private val binding by viewBinding(FragmentBlinkyBinding::bind)

    override val dispatcher: EventDispatcher by this::viewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textViewInfoAddress.text = args.deviceAddress

        binding.buttonToggleDiode.setOnClickListener {
            viewModel.toggleDiode()
        }

        viewModel.startConnection()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                if (!Permissions.isConnectPermissionGranted(requireContext())) {
                    handlePermissionNotGranted()
                }
                observeBluetoothAdapterState(this)
                observeConnectionStatus(this)
                observeDiodeState(this)
                observeButtonState(this)
                subscribe(this)
            }
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.RESUMED) {
                if (!Permissions.isConnectPermissionGranted(requireContext())) {
                    handlePermissionNotGranted()
                }
            }
        }
    }

    private fun observeBluetoothAdapterState(scope: CoroutineScope) {
        bluetoothStateViewModel.adapterState
            .onEach {
                if (it != BluetoothAdapterState.ON) {
                    handleBluetoothDisabled()
                }
            }.launchIn(scope)
    }

    private fun observeConnectionStatus(scope: CoroutineScope) {
        viewModel.uiState.map { it.status }
            .onEach {
                binding.textViewInfoConnectionStatus.setText(
                    when (it) {
                        ConnectionStatus.Connecting -> R.string.connection_status_connecting
                        ConnectionStatus.Discovering -> R.string.connection_status_discovering
                        ConnectionStatus.Ready -> R.string.connection_status_ready
                        ConnectionStatus.Error -> R.string.connection_status_error
                    }
                )
            }.launchIn(scope)
    }

    private fun observeDiodeState(scope: CoroutineScope) {
        viewModel.uiState.map { it.diodeState }
            .onEach {
                binding.buttonToggleDiode.apply {
                    setText(
                        when (it) {
                            DiodeState.DISABLING,
                            DiodeState.ENABLED -> R.string.action_turn_off_diode
                            else -> R.string.action_turn_on_diode
                        }
                    )
                    isEnabled = when (it) {
                        DiodeState.ENABLED,
                        DiodeState.DISABLED -> true
                        else -> false
                    }
                }
                binding.textViewDiodeState.setText(
                    when (it) {
                        DiodeState.ENABLED -> R.string.diode_state_on
                        DiodeState.ENABLING -> R.string.diode_state_turning_on
                        DiodeState.DISABLING -> R.string.diode_state_turning_off
                        DiodeState.DISABLED -> R.string.diode_state_off
                        DiodeState.UNKNOWN -> R.string.getting_diode_state
                    }
                )
                binding.imageViewDiodeState.setImageResource(
                    when (it) {
                        DiodeState.DISABLING,
                        DiodeState.ENABLED -> R.drawable.ic_lightbulb_on_24
                        else -> R.drawable.ic_lightbulb_off_24
                    }
                )
            }.launchIn(scope)
    }

    private fun observeButtonState(scope: CoroutineScope) {
        viewModel.uiState.map { it.buttonState }
            .onEach {
                binding.textViewButtonState.setText(
                    when (it) {
                        ButtonState.PRESSED -> R.string.button_state_pressed
                        ButtonState.RELEASED -> R.string.button_state_released
                        ButtonState.UNKNOWN -> R.string.getting_button_state
                    }
                )
                binding.imageViewButtonState.setImageResource(
                    when (it) {
                        ButtonState.PRESSED -> R.drawable.ic_electric_switch_closed_24
                        else -> R.drawable.ic_electric_switch_open_24
                    }
                )
            }.launchIn(scope)
    }

    override fun handleEvent(event: Event) {
        when (event) {
            is BlinkyViewModel.Companion.BluetoothDisabled -> handleBluetoothDisabled()
        }
        super.handleEvent(event)
    }

    private fun handleBluetoothDisabled() {
        Toast.makeText(requireContext(), R.string.bluetooth_disabled_rationale, Toast.LENGTH_SHORT)
            .show()
        findNavController().navigateUp()
    }

    private fun handlePermissionNotGranted() {
        Toast.makeText(
            requireContext(),
            R.string.bluetooth_connect_permission_rationale,
            Toast.LENGTH_SHORT
        ).show()
        findNavController().navigateUp()
    }
}