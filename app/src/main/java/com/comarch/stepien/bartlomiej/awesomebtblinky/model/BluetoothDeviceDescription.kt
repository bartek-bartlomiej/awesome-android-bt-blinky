package com.comarch.stepien.bartlomiej.awesomebtblinky.model

data class BluetoothDeviceDescription(val displayName: String, val address: String)

