package com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky

enum class ConnectionStatus {
    Connecting,
    Discovering,
    Ready,
    Error
}