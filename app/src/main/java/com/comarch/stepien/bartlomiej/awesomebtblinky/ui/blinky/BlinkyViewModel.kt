package com.comarch.stepien.bartlomiej.awesomebtblinky.ui.blinky

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.comarch.stepien.bartlomiej.awesomebtblinky.dao.BlinkyAdapter
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.Event
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.EventBus
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.EventDispatcher
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky.BlinkyState
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky.ConnectionStatus
import com.comarch.stepien.bartlomiej.awesomebtblinky.repository.BluetoothRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class BlinkyViewModel @Inject constructor(
    private val bluetoothRepository: BluetoothRepository,
    private val bus: EventBus,
    savedStateHandle: SavedStateHandle
) : ViewModel(), EventDispatcher by bus {

    private val _uiState = MutableStateFlow(BlinkyState())
    val uiState: StateFlow<BlinkyState> = _uiState.asStateFlow()

    private val args = BlinkyFragmentArgs.fromSavedStateHandle(savedStateHandle)

    private lateinit var adapter: BlinkyAdapter

    @ExperimentalCoroutinesApi
    fun startConnection() {
        bluetoothRepository.getBlinkyAdapter(args.deviceAddress)?.let {
            adapter = it
            connect()
        } ?: run {
            bus.emitEvent(BluetoothDisabled)
        }
    }

    @ExperimentalCoroutinesApi
    private fun connect() {
        viewModelScope.launch {
            adapter.state
                .catch { cause -> cancel("Connection with Blinky failed", cause) }
                .collect { state ->
                    _uiState.update { state }
                }
        }.also { job ->
            job.invokeOnCompletion {
                it?.let {
                    _uiState.update { state -> state.copy(status = ConnectionStatus.Error) }
                    reconnect()
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun reconnect() {
        viewModelScope.launch {
            delay(300)
            connect()
        }
    }

    fun toggleDiode() {
        adapter.toggleDiode()
    }

    companion object {
        object BluetoothDisabled : Event()
    }
}