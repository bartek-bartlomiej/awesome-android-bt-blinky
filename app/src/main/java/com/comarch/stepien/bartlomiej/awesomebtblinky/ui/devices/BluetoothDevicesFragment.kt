package com.comarch.stepien.bartlomiej.awesomebtblinky.ui.devices

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.comarch.stepien.bartlomiej.awesomebtblinky.R
import com.comarch.stepien.bartlomiej.awesomebtblinky.databinding.FragmentDevicesBinding
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.Event
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.EventDispatcher
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.EventHandler
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.BluetoothAdapterState
import com.comarch.stepien.bartlomiej.awesomebtblinky.permissions.Permissions
import com.comarch.stepien.bartlomiej.awesomebtblinky.ui.BluetoothAdapterStateViewModel
import com.markodevcic.peko.Peko
import com.markodevcic.peko.PermissionResult
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class BluetoothDevicesFragment : Fragment(R.layout.fragment_devices), EventHandler {
    private val bluetoothStateViewModel: BluetoothAdapterStateViewModel by activityViewModels()
    private val viewModel: BluetoothDevicesViewModel by viewModels()

    private val binding by viewBinding(FragmentDevicesBinding::bind)

    override val dispatcher: EventDispatcher by this::viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val devicesAdapter = BluetoothDevicesAdapter()
        binding.availableDevicesList.adapter = devicesAdapter

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                observeAvailableDeviceItems(devicesAdapter, this)
                observeScanningState(this)
                observeAdapterState(this)
                subscribe(this)
            }
        }
    }

    private fun observeAvailableDeviceItems(
        devicesAdapter: BluetoothDevicesAdapter,
        scope: CoroutineScope
    ) {
        viewModel.uiState.map { it.availableDeviceItems }
            .onEach { devicesAdapter.submitList(it) }
            .launchIn(scope)
    }

    private fun observeScanningState(scope: CoroutineScope) {
        viewModel.uiState.map { it.scanning }
            .onEach { activity?.invalidateOptionsMenu() }
            .launchIn(scope)
    }

    private fun observeAdapterState(scope: CoroutineScope) {
        bluetoothStateViewModel.adapterState
            .onEach {
                when (it) {
                    BluetoothAdapterState.ON -> {
                        binding.enableBluetoothPrompt.visibility = View.GONE
                        binding.devicesListLayout.visibility = View.VISIBLE
                    }
                    else -> {
                        binding.enableBluetoothPrompt.visibility = View.VISIBLE
                        binding.devicesListLayout.visibility = View.GONE
                        viewModel.stopScanning()
                    }
                }
                activity?.invalidateOptionsMenu()
            }
            .launchIn(scope)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_bluetooth_devices, menu)

        val adapterState = bluetoothStateViewModel.adapterState.value
        val scanning = viewModel.uiState.value.scanning

        menu.findItem(R.id.action_start_scan).run {
            isVisible = adapterState == BluetoothAdapterState.ON && !scanning
        }
        menu.findItem(R.id.actions_stop_scan).run {
            isVisible = adapterState == BluetoothAdapterState.ON && scanning
            isEnabled = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_start_scan -> {
                scanDevices()
                true
            }
            R.id.actions_stop_scan -> {
                item.isEnabled = false
                viewModel.stopScanning()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun scanDevices() {
        viewLifecycleOwner.lifecycleScope.launch {
            val result =
                Peko.requestPermissionsAsync(requireContext(), Permissions.scanPermission)

            if (result is PermissionResult.Granted) {
                viewModel.startScanning()
            } else {
                Toast.makeText(
                    requireContext(),
                    R.string.bluetooth_scan_permission_rationale,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun handleEvent(event: Event) {
        when (event) {
            is BluetoothDevicesViewModel.Companion.ConnectionRequested ->
                handleConnectionRequestedEvent(event.address)
        }
        super.handleEvent(event)
    }

    private fun handleConnectionRequestedEvent(address: String) {
        findNavController().navigate(
            BluetoothDevicesFragmentDirections.actionBLEDeviceListFragmentToBlinkyFragment(
                address
            )
        )
    }
}
