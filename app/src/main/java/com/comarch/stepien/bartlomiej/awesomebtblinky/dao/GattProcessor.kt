package com.comarch.stepien.bartlomiej.awesomebtblinky.dao

import android.annotation.SuppressLint
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import java.util.*
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

// shamelessly borrowed from EFR
class GattProcessor {
    private val commands: Queue<GattCommand> = LinkedList()
    private val lock: Lock = ReentrantLock()
    private var processing = false

    fun queueRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
        queue(GattCommand(GattCommand.Type.Read, gatt, characteristic))
    }

    fun queueWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
        queue(GattCommand(GattCommand.Type.Write, gatt, characteristic))
    }

    fun queueNotify(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
        queue(GattCommand(GattCommand.Type.Notify, gatt, characteristic))
    }

    private fun queue(command: GattCommand) {
        lock.lock()
        try {
            commands.add(command)
            if (!processing) {
                processNextCommand()
            }
        } finally {
            lock.unlock()
        }
    }

    @SuppressLint("MissingPermission") // checking is handled in PermissionsHelper and UI
    private fun processNextCommand() {
        var success = false
        val command = commands.poll()

        if (command?.gatt != null && command.characteristic != null) {
            val gatt = command.gatt
            val characteristic = command.characteristic

            success = when (command.type) {
                GattCommand.Type.Read -> gatt.readCharacteristic(characteristic)
                GattCommand.Type.Write -> gatt.writeCharacteristic(characteristic)
                GattCommand.Type.Notify -> {
                    var written = false
                    if (gatt.setCharacteristicNotification(characteristic, true)) {
                        val descriptor =
                            characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR)
                        descriptor?.run {
                            value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                            written = gatt.writeDescriptor(this)
                        }
                    }
                    written
                }
            }
        }
        processing = success
    }

    fun handleCommandProcessed() {
        lock.lock()
        try {
            if (commands.isEmpty()) {
                processing = false
            } else {
                processNextCommand()
            }
        } finally {
            lock.unlock()
        }
    }

    private class GattCommand(
        val type: Type,
        val gatt: BluetoothGatt?,
        val characteristic: BluetoothGattCharacteristic?
    ) {
        enum class Type {
            Read, Write, Notify
        }
    }

    companion object {
        private val CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR =
            UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
    }
}
