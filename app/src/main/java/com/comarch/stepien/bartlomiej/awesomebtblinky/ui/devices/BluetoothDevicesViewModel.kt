package com.comarch.stepien.bartlomiej.awesomebtblinky.ui.devices

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.Event
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.EventDispatcher
import com.comarch.stepien.bartlomiej.awesomebtblinky.eventBus.EventBus
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.BluetoothDeviceDescription
import com.comarch.stepien.bartlomiej.awesomebtblinky.repository.BluetoothRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.LinkedHashMap
import javax.inject.Inject

@HiltViewModel
class BluetoothDevicesViewModel @Inject constructor(
    private val bluetoothRepository: BluetoothRepository,
    private val bus: EventBus
) : ViewModel(), EventDispatcher by bus {

    private val _uiState = MutableStateFlow(UiState())
    val uiState: StateFlow<UiState> = _uiState.asStateFlow()

    private var currentScan: Job? = null

    @ExperimentalCoroutinesApi
    fun startScanning() {
        currentScan = viewModelScope.launch {
            _uiState.update { state ->
                state.copy(scanning = true, availableDeviceItems = listOf())
            }

            val deviceMap = linkedMapOf<String, BluetoothDeviceDescription>()

            bluetoothRepository.blinkyDevices
                .catch { cause -> cancel("Scanning failed", cause) }
                .collect { description ->
                    val mapChanged = deviceMap.appendIfAbsent(description.address, description)
                    if (!mapChanged) return@collect

                    val itemState = ItemUiState(
                        displayName = description.displayName,
                        address = description.address,
                        onClick = {
                            stopScanning()
                            bus.emitEvent(ConnectionRequested(description.address))
                        }
                    )
                    _uiState.update { state ->
                        state.copy(availableDeviceItems = state.availableDeviceItems + itemState)
                    }
                }
        }.also { job ->
            job.invokeOnCompletion {
                _uiState.update { state -> state.copy(scanning = false) }
            }
        }
    }

    private fun <K, V> LinkedHashMap<K, V>.appendIfAbsent(key: K, value: V): Boolean {
        val previous = when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> this.putIfAbsent(
                key,
                value
            )
            else -> this[key] ?: this.put(key, value)
        }
        return previous == null
    }

    fun stopScanning() {
        currentScan?.cancel()
    }

    data class UiState(
        val scanning: Boolean = false,
        val availableDeviceItems: List<ItemUiState> = listOf(),
    )

    data class ItemUiState(
        val displayName: String,
        val address: String,
        val onClick: () -> Unit
    )

    companion object {
        data class ConnectionRequested(val address: String) : Event()
    }
}
