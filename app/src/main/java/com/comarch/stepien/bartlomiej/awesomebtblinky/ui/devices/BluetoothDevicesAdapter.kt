package com.comarch.stepien.bartlomiej.awesomebtblinky.ui.devices

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.comarch.stepien.bartlomiej.awesomebtblinky.databinding.ListItemBluetoothDeviceBinding

class BluetoothDevicesAdapter
    : ListAdapter<BluetoothDevicesViewModel.ItemUiState, BluetoothDeviceViewHolder>(
    BluetoothDeviceDiffCallback
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BluetoothDeviceViewHolder =
        ListItemBluetoothDeviceBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
            .run { BluetoothDeviceViewHolder(this) }

    override fun onBindViewHolder(holder: BluetoothDeviceViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

object BluetoothDeviceDiffCallback :
    DiffUtil.ItemCallback<BluetoothDevicesViewModel.ItemUiState>() {
    override fun areItemsTheSame(
        oldItem: BluetoothDevicesViewModel.ItemUiState,
        newItem: BluetoothDevicesViewModel.ItemUiState
    ) =
        oldItem.address == newItem.address

    override fun areContentsTheSame(
        oldItem: BluetoothDevicesViewModel.ItemUiState,
        newItem: BluetoothDevicesViewModel.ItemUiState
    ) =
        oldItem.displayName == newItem.displayName
}
