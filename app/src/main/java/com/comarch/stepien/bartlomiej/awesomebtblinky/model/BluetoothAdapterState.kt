package com.comarch.stepien.bartlomiej.awesomebtblinky.model

enum class BluetoothAdapterState {
    OFF,
    TURNING_OFF,
    TURNING_ON,
    ON
}