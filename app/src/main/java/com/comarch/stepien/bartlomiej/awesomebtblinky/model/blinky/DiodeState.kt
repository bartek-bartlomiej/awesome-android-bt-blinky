package com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky

enum class DiodeState {
    ENABLED,
    ENABLING,
    DISABLING,
    DISABLED,
    UNKNOWN;

    companion object {
        fun fromResult(result: Byte) =
            when (result) {
                1.toByte() -> ENABLED
                0.toByte() -> DISABLED
                else -> UNKNOWN
            }
    }
}