package com.comarch.stepien.bartlomiej.awesomebtblinky.model.blinky

data class BlinkyState(
    val status: ConnectionStatus = ConnectionStatus.Connecting,
    val diodeState: DiodeState = DiodeState.UNKNOWN,
    val buttonState: ButtonState = ButtonState.UNKNOWN
)
