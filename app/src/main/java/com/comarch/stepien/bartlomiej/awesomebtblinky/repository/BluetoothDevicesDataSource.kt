package com.comarch.stepien.bartlomiej.awesomebtblinky.repository

import android.annotation.SuppressLint
import android.bluetooth.le.*
import android.content.Context
import com.comarch.stepien.bartlomiej.awesomebtblinky.dao.BlinkyAdapter
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.BluetoothDeviceDescription
import com.comarch.stepien.bartlomiej.awesomebtblinky.permissions.Permissions
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class BluetoothDevicesDataSource(
    private val applicationContext: Context
) {

    private val bluetoothAdapter by lazy { applicationContext.bluetoothAdapter }

    @SuppressLint("MissingPermission") // checking is handled in PermissionsHelper and UI
    @ExperimentalCoroutinesApi
    fun getDevices(filters: List<ScanFilter>): Flow<BluetoothDeviceDescription> = callbackFlow {
        if (!(bluetoothAdapter != null && bluetoothAdapter!!.isEnabled)) {
            close(BluetoothDisabledException)
        }
        if (!Permissions.isScanPermissionGranted(applicationContext)) {
            close(Permissions.ScanPermissionMissingException)
        }

        val scanCallback = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult) {
                if (isClosedForSend || result.device == null) return
                val description = with(result.device) {
                    val displayName = when {
                        Permissions.isConnectPermissionGranted(applicationContext) -> name
                        else -> null
                    }

                    BluetoothDeviceDescription(displayName = displayName ?: "", address = address)
                }
                trySend(description)
            }
        }

        val scanner = bluetoothAdapter!!.bluetoothLeScanner

        scanner.startScan(filters, SCAN_SETTINGS, scanCallback)
        launch {
            delay(SCAN_PERIOD)
            close()
        }

        awaitClose {
            scanner.stopScan(scanCallback)
        }
    }

    fun getDeviceAdapter(address: String): BlinkyAdapter? =
        bluetoothAdapter?.getRemoteDevice(address)?.let { device ->
            BlinkyAdapter(applicationContext, device)
        }

    companion object {
        private val SCAN_SETTINGS = ScanSettings.Builder().run { build() }
        private val SCAN_PERIOD: Long =
            TimeUnit.MILLISECONDS.convert(10, TimeUnit.SECONDS)

        object BluetoothDisabledException : Exception("Bluetooth is disabled")
    }
}
