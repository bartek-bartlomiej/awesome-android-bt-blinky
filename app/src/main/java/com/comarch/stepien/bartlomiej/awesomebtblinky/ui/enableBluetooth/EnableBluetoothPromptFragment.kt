package com.comarch.stepien.bartlomiej.awesomebtblinky.ui.enableBluetooth

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import by.kirich1409.viewbindingdelegate.viewBinding
import com.comarch.stepien.bartlomiej.awesomebtblinky.R
import com.comarch.stepien.bartlomiej.awesomebtblinky.databinding.FragmentEnableBluetoothPromptBinding
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.BluetoothAdapterState
import com.comarch.stepien.bartlomiej.awesomebtblinky.permissions.Permissions
import com.comarch.stepien.bartlomiej.awesomebtblinky.ui.BluetoothAdapterStateViewModel
import com.markodevcic.peko.Peko
import com.markodevcic.peko.PermissionResult
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class EnableBluetoothPromptFragment : Fragment(R.layout.fragment_enable_bluetooth_prompt) {
    private val bluetoothStateViewModel: BluetoothAdapterStateViewModel by activityViewModels()

    private val binding by viewBinding(FragmentEnableBluetoothPromptBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.enableBluetoothButton.setOnClickListener {
            enableBluetooth()
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                observeAdapterState(this)
            }
        }
    }

    private fun observeAdapterState(scope: CoroutineScope) {
        bluetoothStateViewModel.adapterState
            .onEach {
                binding.enableBluetoothButton.isEnabled = when (it) {
                    BluetoothAdapterState.OFF -> true
                    else -> false
                }
            }
            .launchIn(scope)
    }

    private fun enableBluetooth() {
        viewLifecycleOwner.lifecycleScope.launch {
            Permissions.connectPermission?.let {
                val result = Peko.requestPermissionsAsync(requireContext(), it)

                if (result is PermissionResult.Granted) {
                    bluetoothStateViewModel.enable()
                } else {
                    Toast.makeText(
                        requireContext(),
                        R.string.bluetooth_connect_permission_rationale,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } ?: run {
                bluetoothStateViewModel.enable()
            }
        }
    }
}