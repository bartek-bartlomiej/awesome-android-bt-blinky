package com.comarch.stepien.bartlomiej.awesomebtblinky.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.comarch.stepien.bartlomiej.awesomebtblinky.model.BluetoothAdapterState
import com.comarch.stepien.bartlomiej.awesomebtblinky.repository.BluetoothRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class BluetoothAdapterStateViewModel @Inject constructor(
    private val bluetoothRepository: BluetoothRepository
) : ViewModel() {

    @ExperimentalCoroutinesApi
    val adapterState: StateFlow<BluetoothAdapterState> = bluetoothRepository.adapterState
        .stateIn(
            viewModelScope,
            SharingStarted.Lazily,
            // proper initial value is sent in callbackFlow
            BluetoothAdapterState.OFF
        )

    fun enable() {
        bluetoothRepository.enableBluetooth()
    }
}