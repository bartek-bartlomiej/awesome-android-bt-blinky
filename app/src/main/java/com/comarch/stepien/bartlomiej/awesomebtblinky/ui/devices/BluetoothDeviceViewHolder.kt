package com.comarch.stepien.bartlomiej.awesomebtblinky.ui.devices

import androidx.recyclerview.widget.RecyclerView
import com.comarch.stepien.bartlomiej.awesomebtblinky.R
import com.comarch.stepien.bartlomiej.awesomebtblinky.databinding.ListItemBluetoothDeviceBinding

class BluetoothDeviceViewHolder(
    private val binding: ListItemBluetoothDeviceBinding,
) : RecyclerView.ViewHolder(binding.root) {

    private var currentState: BluetoothDevicesViewModel.ItemUiState? = null

    init {
        binding.root.setOnClickListener {
            currentState?.onClick?.invoke()
        }
    }

    fun bind(state: BluetoothDevicesViewModel.ItemUiState) {
        currentState = state

        val name: String = state.displayName
        val address: String = state.address

        binding.name.text = when {
            name.isNotBlank() -> name
            else -> itemView.context.getString(R.string.unknown_device_display_name)
        }
        binding.address.text = address
    }
}
